import * as React from 'react';
import { Header } from './component/Header';
import { Content } from './component/Content';
import { ApiService } from './service/ApiService';
import { HtmlService } from './service/HtmlService';
import { FilterService } from './service/FilterService';

class App extends React.Component<{}, any> {
  service = new ApiService();
  filterService = new FilterService();
  htmlService = new HtmlService();

  constructor() {
    super();
    this.state = { eventdata: {}, mediadata: {} };
    this.changeState();
  }

  events: Array<string> = ['ifa16', 'womensmarch', 'turnfest17', 'superbooth16', 'bitstreammusic', 'tomorrow2016','LNDWberlin16', 'flimmeTV', 'mevango','mwc17', 'eurocup', 'umf2017', 'huntingSA', 'SocialLive', 'sonnenschein', 'WCD17','covfefe', 'oaf17', 'history'];
  eventCount = 0;
  currentEvent = this.events[this.eventCount];
  changeEvent(direction: boolean) {
    direction ? this.eventCount++ : this.eventCount--;
    if (this.eventCount >= this.events.length) { this.eventCount = 0; }
    if (this.eventCount < 0) { this.eventCount = this.events.length - 1; }
    this.currentEvent = this.events[this.eventCount];
    this.changeState();
  }

  changeState() {
    this.changeStateEvent();
    this.changeStateMedia(0);
  }

  changeStateEvent() {
    this.service.getEventData(this.currentEvent).then((event) => {
      this.setState({ eventdata: event.data });
    });
  }

  changeStateMedia(id: Number) {
    console.log(id)
    this.service.getMediaData(this.currentEvent).then((media) => {
      let filteredObjs: any
      if(id == 1){
        filteredObjs = this.filterService.onlyInsta(media);
        this.setState({ mediadata: filteredObjs});
      }else if(id == 2){
        filteredObjs = this.filterService.onlyFacebook(media);
        this.setState({ mediadata: filteredObjs });
      }else if(id == 3){
        filteredObjs = this.filterService.onlyTwitter(media);
        this.setState({ mediadata: filteredObjs });
      }else if(id == 4){
        filteredObjs = this.filterService.onlyYoutube(media);
        this.setState({ mediadata: filteredObjs });
      }else if(id == 5){
        filteredObjs = this.filterService.onlyFlimme(media);
        this.setState({ mediadata: filteredObjs });
      }else{
        this.setState({ mediadata: media.data });
      }
      // let filteredObjs = this.filterService.onlyInsta(media);
      //this.setState({ mediadata: media.data });
      // console.log(media.data);
    });
  }

  render() {
    let eventdata = this.state.eventdata || [];
    let mediadata = this.state.mediadata || [];
    let header = this.htmlService.getSpinLoader();
    let content = this.htmlService.getSpinLoader();
    let mainStyle = { backgroundColor: '#000000' };
    try {
      eventdata.color.primary;
      mainStyle = { backgroundColor: '#' + eventdata.color.background };
      header = <Header data={eventdata} changeEvent={this.changeEvent.bind(this)} change = {this.changeStateMedia.bind(this)} />;
      content = <Content data={mediadata} />;
    } catch (err) {
      err;
    }
    return (
      <div className="App" style={mainStyle}>
        {header}
        {content}
      </div>
    );
  }
}

export default App;
