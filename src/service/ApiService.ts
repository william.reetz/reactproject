import axios from 'axios';

export class ApiService {

  public getEventData(eventName: string) {
    return axios.get('https://api.flimme.com/api/v1/events/' + eventName);
  }

  public getMediaData(eventName: string) {
    return axios.get('https://api.flimme.com/api/v1/events/' + eventName + '/media');
  }

}