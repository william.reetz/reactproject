export class FilterService {

    /*  
     * Bekommt ein JSON-Objekt und returniert ein neues JSON-Objekt, dass nur Bilder enthaellt
     */


    public onlyImages(object: any) {
        var result = [];
        for (var i = 0; i < object.data.length; i++) {
            if (object.data[i].content_type === "image") {
                result.push(object.data[i]);
            }
        }
        return result;
    }

    public onlyVideos(object: any) {
        var result = [];
        for (var i = 0; i < object.data.length; i++) {
            if (object.data[i].content_type === "video") {
                result.push(object.data[i]);
            }
        }
        return result;
    }

    public onlyInsta(object: any) {
        var result = [];
        for (var i = 0; i < object.data.length; i++) {
            if (object.data[i].service === "instagram") {
                result.push(object.data[i]);
            }
        }
        return result;
    }

    public onlyTwitter(object: any) {
        var result = [];
        for (var i = 0; i < object.data.length; i++) {
            if (object.data[i].service === "twitter") {
                result.push(object.data[i]);
            }
        }
        return result;
    }

    public onlyFacebook(object: any) {
        var result = [];
        for (var i = 0; i < object.data.length; i++) {
            if (object.data[i].service === "facebook") {
                result.push(object.data[i]);
            }
        }
        return result;
    }

    public onlyYoutube(object: any) {
        var result = [];
        for (var i = 0; i < object.data.length; i++) {
            if (object.data[i].service === "youtube") {
                result.push(object.data[i]);
            }
        }
        return result;
    }

    public onlyFlimme(object: any) {
        var result = [];
        for (var i = 0; i < object.data.length; i++) {
            if (object.data[i].service === "flimme") {
                result.push(object.data[i]);
            }
        }
        return result;
    }



    public extendsText(object: any, text: string) {
        //@TODO: Implementieren!
        return null;
    }

    // weitere Filter ...

}