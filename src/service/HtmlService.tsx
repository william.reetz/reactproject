import * as React from 'react';

export class HtmlService{
    public getServicePic(service: string){
        let servicepic = <i className="fa fa-image" />;
        switch (service) {
            case "twitter": servicepic = <i className="fa fa-twitter" />; break;
            case "instagram": servicepic = <i className="fa fa-instagram" />; break;
            case "youtube": servicepic = <i className="fa fa-youtube" />; break;
            case "flimme": servicepic = <span className="emt-flimme"></span>; break;
        }
        return servicepic;
    }
    public getSpinLoader(){
        return <i className="fa fa-spinner fa-pulse fa-fw" />;
    }
}