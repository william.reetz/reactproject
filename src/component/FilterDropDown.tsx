import * as React from 'react';
export class FilterDropDown extends React.Component<any, any> {

    constructor(props: any) {
        super(props);
    }


    render() {
        let fstyle = this.props.fstyle;

        let filterstyle = {
            color: fstyle.color,
            borderColor: fstyle.borderColor
        };
        return (
            <div className="dropdown-content" >
                <span className="fa-stack fa-1x" style={filterstyle} onClick={() => this.props.data(0)}>
                    <i className="fa fa-circle-thin fa-stack-2x fa-inverse" style={filterstyle} />
                    <i className="fa fa-filter fa-stack-1x fa-inverse" style={filterstyle} />
                </span>
                <span className="fa-stack fa-1x" style={filterstyle} onClick={() => this.props.data(5)}>
                    <i className="fa fa-circle-thin fa-stack-2x fa-inverse" style={filterstyle} />
                    <i className="emt-flimme fa-stack-1x fa-inverse" style={filterstyle} />
                </span>
                <span className="fa-stack fa-1x" style={filterstyle} onClick={() => this.props.data(2)}>
                    <i className="fa fa-circle-thin fa-stack-2x fa-inverse" style={filterstyle} />
                    <i className="fa fa-facebook fa-stack-1x fa-inverse" style={filterstyle} />
                </span>
                <span className="fa-stack fa-1x" style={filterstyle} onClick={() => this.props.data(1)}>
                    <i className="fa fa-circle-thin fa-stack-2x fa-inverse" style={filterstyle} />
                    <i className="fa fa-instagram fa-stack-1x fa-inverse" style={filterstyle} />
                </span>
                <span className="fa-stack fa-1x" style={filterstyle} onClick={() => this.props.data(4)}>
                    <i className="fa fa-circle-thin fa-stack-2x fa-inverse" style={filterstyle} />
                    <i className="fa fa-youtube fa-stack-1x fa-inverse" style={filterstyle} />
                </span>
                <span className="fa-stack fa-1x" style={filterstyle} onClick={() => this.props.data(3)}>
                    <i className="fa fa-circle-thin fa-stack-2x fa-inverse" style={filterstyle} />
                    <i className="fa fa-twitter fa-stack-1x fa-inverse" style={filterstyle} />
                </span>

            </div>

        );
    }
}

