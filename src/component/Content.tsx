import * as React from 'react';
import { Media } from './Media';
import { ModalCont } from '../interfaces/ModalCont';
import { HtmlService } from '../service/HtmlService';

export class Content extends React.Component<any, any> {
    htmlService: HtmlService = new HtmlService();
    videoOrImage: any;
    constructor(props: any) {
        super(props);
        this.state = {
            modalContent: {
                backgroundColor: "none",
                fontColor: "none",
                username: "none",
                description: "none",
                contenttype: "none",
                mediaUrl: "none",
                service: "none",
                video: "none"
            }
        };
    }

    setModalContent(moco: ModalCont) {
        this.setState({ modalContent: moco });
    }
    resetVideoOrImage() {
        this.setModalContent(
            {
                backgroundColor: "string",
                fontColor: "string",
                username: "string",
                contenttype: "string",
                mediaUrl: "string",
                service: "string",
                video: "string"
            }
        );
    }

    render() {
        let modal = <div></div>;
        let servicepic = this.htmlService.getServicePic(this.state.modalContent.service);
        if (this.state.modalContent.contenttype === "video") {
            this.videoOrImage = <iframe src={this.state.modalContent.video}>
                <p>Your browser does not support iframes.</p>
            </iframe>
            if (this.state.modalContent.service === "flimme") {
                this.videoOrImage = <img className="modalimg" src={this.state.modalContent.mediaUrl} />
            }

        } else if (this.state.modalContent.contenttype === "image") {
            this.videoOrImage = <img className="modalimg" src={this.state.modalContent.mediaUrl} />
        } else {
            this.videoOrImage = <div></div>
        }

        modal =
            <div id="myModal" className="modal fade" role="dialog" ref="modal">
                <div className="modal-dialog modal-lg">

                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal">&times;</button>
                            <h4 className="modal-title">{this.state.modalContent.username} via {servicepic}</h4>
                        </div>
                        <div className="modal-body">
                            {this.videoOrImage}
                            <hr />
                            <div dangerouslySetInnerHTML={{ __html: this.state.modalContent.description }} />
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal" onClick={() => this.resetVideoOrImage()}>Close</button>
                        </div>
                    </div>

                </div>
            </div>



        var indents = [];
        if (this.props.data.length !== 0) {
            for (var i = 0; i < this.props.data.length; i++) {
                indents.push(
                    <Media key={this.props.data[i]._id}
                        data={this.props.data[i]}
                        setModalContent={this.setModalContent.bind(this)}
                    />
                );
            }
        }

        return (
            <div className="container">
                {indents}
                {modal}
            </div>
        );
    }
}

