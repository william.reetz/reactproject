import * as React from 'react';
import { ModalCont } from '../interfaces/ModalCont';
import { HtmlService } from '../service/HtmlService';

export class Media extends React.Component<any, any> {
    htmlService: HtmlService = new HtmlService();

    constructor(props: any) {
        super(props);
        this.state = { mwOpened: false };
    }

    render() {
        let data = this.props.data;
        let image = data.image || 'https://www.fuer-gruender.de/typo3conf/ext/amdienstleister/Resources/Public/Img/default-placeholder.png';
        let username = data.owner.username || "";
        let service = data.service || ""; 
        let description = data.description || "";
        let banane = <div></div>
        let content_type = data.content_type || "";
        let video = data.video || "";
        let moco: ModalCont = {
            backgroundColor: 'string',
            fontColor: 'string',
            username: username,
            description: description,
            contenttype: content_type,
            mediaUrl: image,
            service: service,
            video: video
        }
        if(image === 'https://www.fuer-gruender.de/typo3conf/ext/amdienstleister/Resources/Public/Img/default-placeholder.png'){
            banane = <div className="letterpic"><div className="addMargin1" dangerouslySetInnerHTML={{ __html: description }} /></div>
        }else{
            banane = <img className="mediaimg" src={image}></img>
        }


        let servicepic = this.htmlService.getServicePic(service);

        return (
            <div>
                <div className="square" onClick={() => this.props.setModalContent(moco)}>
                                            <p className="white tl">{username}</p>
                        <p className="white br">{servicepic}</p>
                    <div className="mediacont" data-toggle="modal" data-target="#myModal" >
                        {banane}
                    </div>
                </div>
            </div>
        );
    }
}

