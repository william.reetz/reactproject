import * as React from 'react';
import { FilterDropDown } from './FilterDropDown';

export class Header extends React.Component<any, any> {


    constructor(props: any) {
        super(props);
    }
 


    changeFilter(){
        console.log(this.props.change)
    }

    render() {
        let headerStyle = {
            backgroundColor: '#' + this.props.data.color.primary,
            color: '#' + this.props.data.color.primary_txt,
            borderColor: '#' + this.props.data.color.primary_txt
        };
        let aleftStyle = { float: 'left', cursor: 'pointer', color: '#' + this.props.data.color.primary_txt};
        let arightStyle = { float: 'right', cursor: 'pointer',color: '#' + this.props.data.color.primary_txt };
        let descStyle = { display: 'inline-block', margin: '0 auto' };

        return (
            <div style={headerStyle} className="header">
                <br />
                <div className="container">
                    <i className="fa fa-angle-left fa-inverse fa-2x" style={aleftStyle} onClick={() => this.props.changeEvent(false)} />
                    <i className="fa fa-angle-right fa-inverse fa-2x" style={arightStyle} onClick={() => this.props.changeEvent(true)} />
                    <p style={descStyle}>{this.props.data.description}</p>
                </div>
                <div className="container">
                    <hr  style={headerStyle} />
                    <div className="container">
                        <div className="left">
                            <p className="title" style={headerStyle}>{this.props.data.title}</p>
                        </div>
                        <div className="right">
                            
                            <div className="dropdown">
                                <FilterDropDown data = {this.props.change} fstyle={headerStyle}/>
                            <span className="fa-stack fa-1x" style={headerStyle} onClick={() => this.props.change(0)} >
                                <i className="fa fa-circle-thin fa-stack-2x" />
                                <i className="fa fa-filter fa-stack-1x" />
                            </span>
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

