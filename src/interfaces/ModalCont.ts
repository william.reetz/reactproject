export interface ModalCont{
    backgroundColor: string,
    fontColor: string,
    username: string,
    description?: HTMLElement,
    contenttype: string,
    mediaUrl: string,
    service: string
    video: string
}